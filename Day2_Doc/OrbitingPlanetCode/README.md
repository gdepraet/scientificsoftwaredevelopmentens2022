A simple python code that shows animation of multiple planets orbiting the centre of the coordinate system. The number of planets can be adjusted by changing the  paramter N in the code.

To run the code, type in the terminal:
$ python planet_main.py

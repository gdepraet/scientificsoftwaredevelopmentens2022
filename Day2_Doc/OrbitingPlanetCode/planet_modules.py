"""
The tools needed to build a small solar system and calculate the time evolution 
of it's planets.
"""


import numpy as np


def generate_coordinates(N):
    """
    Generate random coordinates in 2D space.

    This function generates an N by 2 dimensional numpy array of random numbers
    between -1 and 1

    Parameters
    ----------
    N : int
        number of x-y coordinates


    Returns
    -------
    numpy.ndarray
        An N by 2 array of coordinates

    Examples
    --------
    >>> generate_coordinates(1)
    [[ 0.5691428  -0.90499648]]
    >>> generate_coordinates(2)
    [[ 0.8723936  -0.07490176]
     [ 0.7602751  -0.01409078]]
    >>> generate_coordinates(3)
    [[ 0.30093681 -0.41481287]
     [-0.87995954 -0.10437273]
     [-0.79285572 -0.17942596]]
    """
    return np.random.rand(N, 2) * 2 - 1


def generate_velocities(planet_coord):
    """
    Generate random velocities in 2D space.

    This function generates an N by M dimensional numpy array of velocity
    vectors with magnitude 0.01 and a direction perpendicular to the
    direction of the coordinates. N and M are the number of rows and columns of
    planet_coord respectivly.

    Parameters
    ----------
    planet_coord : numpy.ndarray
        N by M array of coordinates

    Returns
    -------
    numpy.ndarray
        N by M array of velocities

    Examples
    --------
    >>> planet_coord = np.asarray([[ 0.8723936,  -0.07490176],
                                   [ 0.7602751,  -0.01409078]])
    >>> velocities = generate_velocities(planet_coord)
    >>> velocities
    [[0.00085543 0.00996334]
     [0.00018531 0.00999828]]
    >>> np.sqrt(np.sum(np.square(velocities),axis=1))
    [0.01 0.01]
    >>> np.sum(velocities*planet_coord, axis=0)
    [1.08420217e-19 0.00000000e+00]
    """
    planet_vel = np.zeros_like(planet_coord, dtype=float)
    planet_vel[:, 0] = np.sqrt(1 / (1 + (planet_coord[:, 0] / planet_coord[:, 1]) ** 2))
    planet_vel[:, 1] = -planet_vel[:, 0] * planet_coord[:, 0] / planet_coord[:, 1]
    planet_vel = planet_vel * 0.01
    return planet_vel


def init(N):
    """
    Generate random coordinates and velocites in 2D space.

    This function generates an two N by 2 dimensional numpy array for
    coordinates and velocities. The coordinates are random numbers between
    -1 and 1, and the velocities are perpindicular to the direction of the
    vector conecting the origin ([0,0]) to the planet coordinates.

    Parameters
    ----------
    N : int
        number of x-y coordinates


    Returns
    -------
    planet_coord: numpy.ndarray
        An N by 2 array of random coordinates

    planet_vel: numpy.ndarray
        An N by 2 array of velocities
    """
    planet_coord = generate_coordinates(N)
    planet_vel = generate_velocities(planet_coord)
    return planet_coord, planet_vel


def get_radius(planet_coord):
    """
    Calculate the Euclidean distance of the coordinate to the origin.

    Parameters
    ----------
    planet_coord : numpy.ndarray
        planet coordinates


    Returns
    -------
    radius: numpy.ndarray
        An N by 1 dimensional array coordinate distances

    Examples
    --------
    >>> planet_coord = np.asarray([[ 0.8723936,  -0.07490176],
                                   [ 0.7602751,  -0.01409078]])
    >>> get_radius(planet_coord)
    [[0.87560314]
     [0.76040567]]
    """
    radius = np.sqrt(np.sum(np.square(planet_coord), axis=1)).reshape(
        planet_coord.shape[0], 1
    )
    return radius


def get_force_direction(planet_coord):
    """
    Calculate the unit vector pointing from the origin to the planet
    coordinates.

    Parameters
    ----------
    planet_coord : numpy.ndarray
        planet coordinates


    Returns
    -------
    unit_vectors: numpy.ndarray
        unit vectors in an array with the same dimension as planet_coord

    Examples
    --------
    >>> planet_coord = np.asarray([[ 0.30355606, -0.22207906],
                                   [-0.081487,    0.65667466],
                                   [-0.28479924, -0.82192082]])
    >>> unit_vectors = get_radius(planet_coord)
    >>> unit_vectors
    [[-0.80707483  0.59044916]
     [ 0.12314587 -0.99238858]
     [ 0.3274064   0.94488362]]
    >>> np.sqrt(np.sum(np.square(unit_vectors),axis=1))
    [1. 1. 1.]

    """
    unit_vectors = -planet_coord / get_radius(planet_coord)
    return unit_vectors


def calculate_acceleration(sun_mass, planet_coord):
    """
    Calculate the acceleration of the planets using Newton's law of gravity,
    a = F/m = G*M/r**2
    Here, M is the mass of the sun, m is the mass of the planet, and r is the
    distance between the sun and the planet. For simplicity we assume the
    gravitational constant is G=1.

    Parameters
    ----------
    planet_coord : numpy.ndarray
        planet coordinates


    Returns
    -------
    sun_mass: float
        Mass used to calculate the gravitational acceleration in Newton's law
        of gravity (M in the above describtion).
    planet_coord: numpy.ndarray
        Planet coordinates
    """
    planet_sun_distance = get_radius(planet_coord)
    planet_sun_direction = get_force_direction(planet_coord)
    acceleration = (sun_mass / planet_sun_distance**2) * planet_sun_direction
    return acceleration


def velocity_verlet(planet_coord, planet_vel, sun_mass):
    """
    Calculates the Velocity Verlet integration for new planet coordinates and
    velocities. This method evolves the position and velocity of objects from
    time t, to time t+1.

    Parameters
    ----------
    planet_coord : numpy.ndarray
        planet coordinates at time t

    planet_vel : numpy.ndarray
        planet velocities at time t

    sun_mass : float
        Mass used to calculate the gravitational acceleration


    Returns
    -------
    planet_coord : numpy.ndarray
        planet coordinates at time t+1

    planet_vel : numpy.ndarray
        planet velocities at time t+1
    """
    acceleration_t = calculate_acceleration(sun_mass, planet_coord)

    # update with acceleration at t
    planet_coord += planet_vel + 0.5 * acceleration_t

    acceleration_tp1 = calculate_acceleration(sun_mass, planet_coord)

    # update with acceleration at t and t+1
    planet_vel += 0.5 * (acceleration_t + acceleration_tp1)
    return planet_coord, planet_vel

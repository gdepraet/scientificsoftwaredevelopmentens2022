import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib.pyplot import cm

from planet_modules import generate_coordinates, generate_velocities, init

# from planet_modules import get_radius, get_force_direction,
from planet_modules import velocity_verlet


def animate(i, planet_coord, planet_vel, sun_mass):

    planet_coord, planet_vel = velocity_verlet(planet_coord, planet_vel, sun_mass)

    for i in range(planet_coord.shape[0]):
        lines[i].set_data(planet_coord[i, 0], planet_coord[i, 1])  # update the data
    return lines


fig, ax = plt.subplots()
ax.set_xlim(-1, 1)
ax.set_ylim(-1, 1)
ax.set_aspect("equal")
ax.plot([0], [0], "o", ms=30, c="gold")

N = 3
planet_coord, planet_vel = init(N)
colors = cm.rainbow(np.linspace(0, 1, N))

lines = []

for c in colors:
    lines.append(ax.plot([], [], ".", ms=20, c=c)[0])


sun_mass = 0.0002

# planet_coord = np.asarray([[ 0.8723936,  -0.07490176],[ 0.7602751,  -0.01409078]])
# velocities = generate_velocities(planet_coord)
# print(velocities)
# print(np.sqrt(np.sum(np.square(velocities),axis=1)))
# print(np.sum(velocities*planet_coord, axis=1))


ani = animation.FuncAnimation(
    fig,
    animate,
    fargs=(planet_coord, planet_vel, sun_mass),
    interval=2,
    blit=True,
)
plt.show()

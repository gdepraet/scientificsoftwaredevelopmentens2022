To reproduce the website we built in class, 

1) follow the slides to install sphinx.
2) run 
```
sphinx-quickstart
```
in this directory.
3) copy the files in the "files" directory and replace the existing files in the
 source directory
4) run 
``` 
make html
```
5) open "index.html" located in the build directory.
